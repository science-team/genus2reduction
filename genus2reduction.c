/* genus2reduction 
   by Qing Liu <liu@math.u-bordeaux.fr>
   and Henri Cohen <cohen@math.u-bordeaux.fr>

   An algorithm '� la Tate' for curves of genus 2

   Current maintainer: William Stein (wstein@ucsd.edu)
   (as part of SAGE: Software for Algebra and Geometry Experimentation)
*/

/* Qing Liu: Last modified october 5th 1998 */ 

/* 
   William Stein: Modified 2006-03-05.

*/

#include "pari.h"

#ifdef LONG_IS_64BIT
#  define VERYBIGINT (9223372036854775807L) /* 2^63-1 */
#  define EXP220 (1099511627776L)          /* 2^40   */
#  define BIGINT (2147483647)              /* 2^31-1 */
#else
#  define VERYBIGINT (2147483647L) /* 2^31-1 */
#  define EXP220 (1048576L)       /* 2^20   */
#  define BIGINT (32767)          /* 2^15-1 */
#endif

GEN
mppgcd(GEN a, GEN b)
{
  if (typ(a) != t_INT || typ(b) != t_INT) err(arither1);
  return gcdii(a,b);
}

GEN
caltheta(GEN pol, GEN p, long lambda);

GEN
calthetazi(GEN polh);

GEN calthetazi2(GEN polh);

GEN 
factmz(GEN polhp, GEN p);

GEN 
polymini(GEN pol, GEN p);

GEN 
polyminizi(GEN pol);

GEN 
polyminizi2(GEN pol);

GEN
zi2mod(GEN u);

long polval(GEN pol, GEN p);

GEN factorpadicnonun(GEN pol, GEN p, long r);

long
discpart(GEN polh, GEN p, long ord);

long
myval(GEN x, GEN p)
{
  return gcmp0(x)?EXP220:ggval(x,p);
}

long
myvalzi(GEN b);

long
myvalzi2(GEN b);

main()
{
  char buf[1000];
  GEN a0,a1,a2,a3,a4,a5,a6,jpol2,jpol4,jpol6,j2,j4,j6,j8,j10,i4,i12,j12,q,p,r,n,m;
  GEN p1,p2,pro,s,pro1,pro2,dk,d1k,d2k,dm,rk,polh,polh1,theta,theta1,polf,list,c1,c2,c3,c4,c5,c6,prod,facti,facto,factp,e0,b,polhp,unmodp;
  GEN apol2,apol3,apol4,apol5,bpol2,A2,A3,A4,A5,B2,e1,e2,e3,vl,vm,vn,b0,b1,jh10,polr,Ieps,sjinv,pjinv,cond,matmin;
  long eps,eps2,i,val[8],deg[8],va0,va2,va3,va4,va5,vb2,vb5,vb6,v12,flc;
  long tt,d,d1,d2,d3,f1,f2,g,h,v1,v2,j2m,r1,r2,r3,r4,dismin,alpha,lambda;
  long indice,vc6,mm,nb,dism,maxc,condp,R,alpha1,comp,beta,vy,dd,pr,ip,j,temp;
  
  init(10000000,500000);
  jpol2=lisseq("a0;a1;a2;a3;a4;a5;a6;2^-2*(-120*a0*a6+20*a1*a5-8*a2*a4+3*a3^2)");
  jpol4=lisseq("a0;a1;a2;a3;a4;a5;a6;2^-7*(240*(a0*a3*a4*a5+a1*a2*a3*a6)-400*(a0*a2*a5^2+a1^2*a4*a6)-64*(a0*a4^3+a2^3*a6)+16*(a1*a3*a4^2+a2^2*a3*a5)-672*a0*a3^2*a6+240*a1^2*a5^2-112*a1*a2*a4*a5-8*a1*a3^2*a5+16*a2^2*a4^2-16*a2*a3^2*a4+3*a3^4+2640*a0^2*a6^2-880*a0*a1*a5*a6+1312*a0*a2*a4*a6)");
  jpol6=lisseq("a0;a1;a2;a3;a4;a5;a6;2^-10*(1600*(a0^2*a4^2*a5^2+a1^2*a2^2*a6^2)+1600*(a0*a1*a2*a5^3+a1^3*a4*a5*a6)+640*(a0*a1*a3*a4*a5^2+a1^2*a2*a3*a5*a6)-4000*(a0^2*a3*a5^3+a1^3*a3*a6^2)-384*(a0*a1*a4^3*a5+a1*a2^3*a5*a6)-640*(a0*a2^2*a4*a5^2+a1^2*a2*a4^2*a6)+80*(a0*a2*a3^2*a5^2+a1^2*a3^2*a4*a6)+192*(a0*a2*a3*a4^2*a5+a1*a2^2*a3*a4*a6)-48*(a0*a3^3*a4*a5+a1*a2*a3^3*a6)-224*(a1^2*a3*a4^2*a5+a1*a2^2*a3*a5^2)+64*(a1^2*a4^4+a2^4*a5^2)-64*(a1*a2*a3*a4^3+a2^3*a3*a4*a5)+16*(a1*a3^3*a4^2+a2^2*a3^3*a5)-4096*(a0^2*a4^3*a6+a0*a2^3*a6^2)+6400*(a0^2*a2*a5^2*a6+a0*a1^2*a4*a6^2)+10560*(a0^2*a3*a4*a5*a6+a0*a1*a2*a3*a6^2)+2624*(a0*a1*a3*a4^2*a6+a0*a2^2*a3*a5*a6)-4432*a0*a1*a3^2*a5*a6-8*a2*a3^4*a4+a3^6-320*a1^3*a5^3+64*a1^2*a2*a4*a5^2+176*a1^2*a3^2*a5^2+128*a1*a2^2*a4^2*a5+112*a1*a2*a3^2*a4*a5-28*a1*a3^4*a5+16*a2^2*a3^2*a4^2+5120*a0^3*a6^3-2544*a0^2*a3^2*a6^2+312*a0*a3^4*a6-14336*a0^2*a2*a4*a6^2+1024*a0*a2^2*a4^2*a6-2560*a0^2*a1*a5*a6^2-2240*a0*a1^2*a5^2*a6-6528*a0*a1*a2*a4*a5*a6-1568*a0*a2*a3^2*a4*a6)");
 labinf:
  printf("   \n");fflush(stdout);
  printf("enter Q(x) : ");fflush(stdout);fflush(stdin);
  scanf("%s",buf);q=lisexpr(buf);
  printf("enter P(x) : ");fflush(stdout);fflush(stdin);
  scanf("%s",buf);p=lisexpr(buf);
  printf(" \n");fflush(stdout);
  polr=gadd(gmul(q,q),gmul2n(p,2)); 
  if(gcmp0(polr)) exit(0);
  if(lgef(polr)>9||lgef(polr)<8)
    {
      printf("It is not a curve of genus 2\n");fflush(stdout);
      goto labinf;
    }
  a0=truecoeff(polr,6);
  a1=truecoeff(polr,5);
  j10=gcmp0(a0)?gmul2n(gmul(gmul(a1,a1),discsr(polr)),-12):gmul2n(discsr(polr),-12);
  if(gcmp0(j10)) {printf("singular curve\n");fflush(stdout);exit(1);}
  facto=factor(gabs(j10, 0));
  temp=timer(); 
  printf("factorization CPU time = %ld\n",temp);fflush(stdout);  
  factp=(GEN)facto[1];
  pr=1; if(!cmpis((GEN)factp[1],2)) pr=2;
  matmin=cgetg(6,19);
  for(j=1;j<=5;j++)
    matmin[j]=lgetg(lg(factp)-pr+1,18);
  for(i=pr;i<lg(factp);i++)
    { 
      p=(GEN)factp[i];
      polf=polymini(polr,p);
      coeff(matmin,i,1)=polf[1];
      coeff(matmin,i,2)=polf[2];
      coeff(matmin,i,3)=polf[3];
      coeff(matmin,i,4)=polf[4];
      coeff(matmin,i,5)=polf[5];
      polr=gmul((GEN)polf[1],gpui(p,(GEN)polf[4],0));
    }
  a0=truecoeff(polr,6);
  a1=truecoeff(polr,5);
  j10=gcmp0(a0)?gmul2n(gmul(gmul(a1,a1),discsr(polr)),-12):gmul2n(discsr(polr),-12);
  for(i=1;i<lg(factp);i++)
    {
      gcoeff(facto,i,2)=stoi(myval(j10,(GEN)factp[i]));
    }
  dd=itos(gmul(gdeux,gdivent(stoi(polval(polr,gdeux)),gdeux)));
  printf("a minimal equation over Z[1/2] is : \n");
  printf("y^2 = ");output(gdiv(polr,gpuigs(gdeux,dd)));
  printf(" \n");fflush(stdout);
  
  printf("factorization of the minimal (away from 2) discriminant : \n");
  output(facto);printf(" \n");fflush(stdout);    
  
  a2=truecoeff(polr,4);a3=truecoeff(polr,3);
  a4=truecoeff(polr,2);a5=truecoeff(polr,1);
  a6=truecoeff(polr,0);
  j2=gsubst(gsubst(gsubst(gsubst(gsubst(gsubst(gsubst(jpol2,1,a0),2,a1),3,a2),4,a3),5,a4),6,a5),7,a6);
  j4=gsubst(gsubst(gsubst(gsubst(gsubst(gsubst(gsubst(jpol4,1,a0),2,a1),3,a2),4,a3),5,a4),6,a5),7,a6);
  i4=gsub(gsqr(j2),gmulsg(24,j4));
  j6=gsubst(gsubst(gsubst(gsubst(gsubst(gsubst(gsubst(jpol6,1,a0),2,a1),3,a2),4,a3),5,a4),6,a5),7,a6);
  j8=gmul2n(gsub(gmul(j2,j6),gmul(j4,j4)),-2);
  i12=gmul2n(gsub(gadd(gsqr(gmul(j2,j4)),gmulsg(36,gmul(gmul(j2,j4),j6))),gadd(gadd(gmulsg(32,gmul(gsqr(j4),j4)),gmul(j6,gmul(gsqr(j2),j2))),gmulsg(108,gsqr(j6)))),-2);
  for(i=1;i<=2;i++) deg[i]=2*i;
  for(i=3;i<=7;i++) deg[i]=2*i-2;
  cond=gun;
  condp=0;
  ip=0;
  p=gun;
  
 recp:
  cond=gmul(cond,gpuigs(p,condp));
  gcoeff(facto,ip,2)=stoi(condp); 
  ip=ip+1;
  if(ip>=lg(factp)) 
    {
      printf("  \n");fflush(stdout);      
      dd=ggval(cond,gdeux);
      if(dd<4)
	{
	  printf("the conductor is ");output(cond);
	  printf("in factorized form : ");output(facto); 
	  temp+=timer();
	  printf("total CPU time = %ld\n",temp);fflush(stdout);  
	  goto labinf;
	}
      cond=gdiv(cond,gpuigs(gdeux,dd));
      printf("the prime to 2 part of the conductor is ");output(cond);
      gcoeff(facto,1,2)=gzero;
      printf("in factorized form : ");
      output(facto);
      temp+=timer();
      printf("total CPU time = %ld\n",temp);fflush(stdout); 
      goto labinf;
    }
  p=(GEN)factp[ip];  
  printf("p=");output(p);
  dismin=ggval(j10,p);
  val[1]=myval(j2,p);val[2]=myval(j4,p);val[3]=myval(i4,p);
  val[4]=myval(j6,p);val[5]=myval(j8,p);val[6]=dismin;
  val[7]=myval(i12,p);
  if(gcmp(p,stoi(5))>0) maxc=4;
  else  
    {
      if(!cmpis(p,5)) maxc=9;
      else
	{
	  if(!cmpis(p,3)) maxc=10;
	  else maxc=20;
	}
    }
/* maxc=maximum du conducteur  */ 
  unmodp=gmodulcp(gun,p);	  
 
  printf("(potential) stable reduction : ");
  p1=cgetg(8,18);
  for(i=1;i<=7;i++) p1[i]=(long)gdiv(stoi(val[i]),stoi(deg[i]));
  s=(GEN)p1[1];for(i=2;i<=7;i++) if(gcmp((GEN)p1[i],s)<0) s=(GEN)p1[i];
  if(gegal(s,(GEN)p1[6])) {tt=1;printf(" (I)\n");goto aff;}
  if(gegal(s,(GEN)p1[7])) 
    {
      tt=2;
      printf(" (II), j=");fflush(stdout);
      output(lift(gmul(unmodp,gdiv(gpuigs(i4,3),i12))));
      goto aff;
    }
  if(gegal(s,(GEN)p1[3]))
  {
    if((val[2]==val[3])||(2*val[4]==3*val[3]))
    {tt=3;printf(" (III)\n");goto aff;}
    else {tt=4;printf(" (IV)\n");goto aff;}
  }
  eps=1;if(gegal(p,gdeux)) eps=4;if(gegal(p,stoi(3))) eps=3;
  eps2=1;if(gegal(p,gdeux)) eps2=5;if(gegal(p,stoi(3))) eps2=4;
  Ieps=gdivgs(j2,12);if(gegal(p,gdeux)) Ieps=j8;if(gegal(p,stoi(3))) Ieps=j6;
  r1=3*eps*val[3];
  r3=eps*val[6]+val[eps2];
  r2=eps*val[7];
  r4=r1;if(r2<r4) r4=r2;if(r3<r4) r4=r3;
  if(r3==r4) 
    {
      tt=5;
      sjinv=lift(gmul(unmodp,gadd(stoi(1728),gdiv(gpuigs(i12,eps),gmul(gpuigs(j10,eps),Ieps)))));
      pjinv=lift(gmul(unmodp,gdiv(gpuigs(i4,3*eps),gmul(gpuigs(j10,eps),Ieps)))); 
      printf(" (V), j1+j2=");fflush(stdout);brute(sjinv,'g',-1);
      printf(", j1*j2=");fflush(stdout);output(pjinv);
      goto aff;
    }
  if(r2==r4) 
    {
      tt=6;
      printf(" (VI), j=");fflush(stdout);
      output(lift(gmul(unmodp,gdiv(gpuigs(i4,3),i12))));
      goto aff;
    }
  if(r1==r4) {tt=7;printf(" (VII)\n");goto aff;}
 aff:
  if(!dismin) 
    {
      printf("good reduction at p : [I{0-0-0}] page 155, (1), f=0\n");
      condp=0;fflush(stdout);goto recp;    
    }
  if(dismin==1) 
    {
      condp=1;
      printf("reduction at p : [I{1-0-0}] page 170, (1), f=1\n");
      fflush(stdout);goto recp;    
    }
  if(dismin==2) 
    {
      switch(tt)
	{
	case 2:
	  {
	    condp=1;
	    printf("reduction at p : [I{2-0-0}] page 170, (2), f=1\n");
	    fflush(stdout);goto recp; 
	  }
	case 3:
	  {
	    condp=2;
	    printf("reduction at p : [I{1-1-0}] page 179, (1), f=2\n");
	    fflush(stdout);goto recp; 
	  }
	case 5:
	  {
	    if(!cmpis(p,2)||!cmpis(p,3))
	      {printf("bug sur tt 1\n");fflush(stdout);exit(1);}   
	    condp=2;
	    printf("reduction at p : [I{0}-II-0] page 159, (1), f=2\n");
	    fflush(stdout);goto recp; 
	  }
	default: {printf("bug sur tt 2\n");fflush(stdout);exit(1);}   
	}
    }
  if(!cmpis(p,2)) {condp=4;goto recp;}
  printf("reduction at p : ");fflush(stdout);  
  polh=gcoeff(matmin,ip,1);  
  lambda=itos(gcoeff(matmin,ip,2));
  theta=gcoeff(matmin,ip,3);
  alpha=itos(gcoeff(matmin,ip,4));
  if(!gcmp0(gcoeff(matmin,ip,5))) goto quadratic;  
  if(gcmp0(theta)&&lambda<=2) 
    {
      if(tt>=5) {printf("bug sur tt 3\n");fflush(stdout);exit(1);}
      goto modere;  
    }
  if(dismin==3) 
    {
      switch(tt)
	{
	case 2: goto modere;
	case 3: condp=2;printf("[I{2-1-0}] page 179, (2), f=2\n");break;
	case 4: condp=2;printf("[I{1-1-1}] page 182, (3), f=2\n");break;
	case 5:
	  {
	    if(cmpis(p,3)||gegal(theta,ghalf))
	      {
		condp=2;printf("[I{0}-III-0] page 161, (2), f=2\n");
	      }
	    else
	      {
		goto labelm3;
	      }
	  }
	  break;
	case 6:
	  {
	    if(!cmpis(p,3)) 
	      {printf("bug de conducteur\n");fflush(stdout);exit(1);}
	    condp=3;
	    printf("[I{1}-II-0] page 172, (1), f=3\n");
	  }
	  break;
	default: printf("bug switch sur tt 4\n");fflush(stdout);exit(1);
	}
      fflush(stdout);goto recp; 
    }
  switch(lambda)
    {
    case 0: 
      {
	switch(itos(gmul(theta, stoi(60)))+alpha)
	  {
	  case 10: condp=dismin-1;printf("[V] page 156, (3), f=%ld\n",condp);
	    fflush(stdout);break;
	  case 11: condp=dismin-11;printf("[V*] page 156, (3), f=%ld\n",condp);
	    fflush(stdout);break;
	  case 12: condp=dismin-2;printf("[IX-2] page 157, (5), f=%ld\n",condp);
	    fflush(stdout);break;
	  case 13: condp=dismin-12;printf("[VIII-4] page 157, (1), f=%ld\n",condp);
	    fflush(stdout);break;
	  case 24: condp=dismin-8;printf("[IX-4] page 158, (5), f=%ld\n",condp);
	    fflush(stdout);break;
	  case 15: case 16: 
	    {
	      if(tt>=5) {printf("bug sur tt 6\n");fflush(stdout);exit(1);}
	      goto modere;
	    }
	  case 20: case 21:
	    {
	      vb5=myval(truecoeff(polh,1),p);vb6=myval(truecoeff(polh,0),p);
	      if(vb6>=3)
		{
		  if(vb5>=3) 
		    {
		      condp=dismin-8;
		      printf("[II*-IV-(-1)] page 164, (3), f=%ld\n",condp);
		      fflush(stdout);
		    }
		  else
		    {
		      if(vb5<2) {printf("bug red1");fflush(stdout);exit(1);} 
		      condp=dismin-7;		      
		      printf("[IV-III*-(-1)] page 167, (6), f=%ld\n",condp);
		      fflush(stdout);
		    }
		}
	      else
		{
		  b=cgetg(8,18);
		  b[1]=(long)truecoeff(polh,6);b[2]=(long)truecoeff(polh,5);
		  b[3]=(long)truecoeff(polh,4);b[4]=(long)truecoeff(polh,3);
		  b[5]=(long)truecoeff(polh,2);b[6]=(long)truecoeff(polh,1);
		  b[7]=(long)truecoeff(polh,0);
  		  if(gdivise((GEN)b[1],p)) 
		    {printf("bug sur b0");fflush(stdout);exit(1);}
		  c1=gmul2n((GEN)b[2],-1);
		  c2=gmul2n(gsub(gmul((GEN)b[1],(GEN)b[3]),gsqr(c1)),-1);
		  c3=gmul2n(gsub(gmul(gsqr((GEN)b[1]),(GEN)b[4]),gmul2n(gmul(c1,c2),1)),-1);
		  c4=gsub(gmul(gpuigs((GEN)b[1],3),(GEN)b[5]),gadd(gmul2n(gmul(c1,c3),1),gsqr(c2)));
		  c5=gsub(gmul(gpuigs((GEN)b[1],4),(GEN)b[6]),gmul2n(gmul(c2,c3),1));
		  c6=gsub(gmul(gpuigs((GEN)b[1],5),(GEN)b[7]),gsqr(c3));
/* on a b0^5*H(x/b0)=(x^3+c1*x^2+c2*x+c3)^2+c4*x^2+c5*x+c6 */
		  vc6=myval(c6,p);
		  if(vc6==2)
		    { 
		      if(alpha) 
			{
			  condp=dismin-16;
			  printf("[IV] page 155, (1), f=%ld\n",condp);fflush(stdout);
			}
		      else  
			{
			  condp=dismin-6;
			  printf("[III] page 155, (3)^2, f=%ld\n", condp);fflush(stdout);
			}
		    }
		  else
		    {
		      if(myval(c3,p)>1) 
			{printf("bug c3");fflush(stdout);exit(1);}
		      mm=min(min(3*myval(c4,p)-4,3*myval(c5,p)-5),3*vc6-6);
		      if(alpha) 
			{
			  condp=dismin-mm-16;
			  printf("[III*{%ld}] page 184, (1), f=%ld\n",mm,condp);fflush(stdout);
			}
		      else  
			{	
			  if(mm%3) 
			    {
			      condp=dismin-mm-6;
			      printf("[III{%ld}] page 184, (9), f=%ld\n",mm,condp);fflush(stdout);
			    }
			  else 
			    {
			      condp=dismin-mm-6;
			      printf("[III{%ld}] page 184, (3)^2, f=%ld\n",mm,condp);fflush(stdout);
			    }
			}
		    }
		}
	    } 
	    break;      
	  case 30: 
	    {
	      if(cmpis(p,3)) goto modere;
	      else goto quartic;
	    }
	    break;	
	  default: printf("bug red2\n");fflush(stdout);exit(1);	   
	  }
      }
      break;
    case 1:
      {
	switch(itos(gmul(theta, stoi(60)))+alpha)
	  {
	  case 12: 
	    condp=dismin;
	    printf("[VIII-1] page 156, (1), f=%ld\n",condp);fflush(stdout);break;
	  case 13: 
	    condp=dismin-10;
	    printf("[IX-3] page 157, (5), f=%ld\n",condp);fflush(stdout);break;
	  case 24: 
	    condp=dismin-4;
	    printf("[IX-1] page 157, (5), f=%ld\n",condp);fflush(stdout);break;
	  case 25: 
	    condp=dismin-14;
	    printf("[VIII-3] page 157, (1), f=%ld\n",condp);fflush(stdout);break;
	  case 36: 
	    condp=dismin-8;
	    printf("[VIII-2] page 157, (1), f=%ld\n",condp);fflush(stdout);break;
	  case 15: 
	    condp=dismin-1;
	    printf("[VII] page 156, (2), f=%ld\n",condp);fflush(stdout);break;
	  case 16: 
	    condp=dismin-11;
	    printf("[VII*] page 156, (2), f=%ld\n",condp);fflush(stdout);break;
	  case 20: 
	    {           
	      if(cmpis(p,3))
		{
		  d=6*val[6]-5*val[7]-2;
		  if(d%6)
		    {
		      printf("bug dans le calcul d'un indice\n");
		      fflush(stdout);exit(1);
		    }
		  dism=(d/6);
		}
	      else
		{
		  list=factorpadicnonun(polh,p,dismin-5);
		  nb=lg(list);prod=polun[varn(polh)];
		  for(i=1;i<nb;i++)
		    {
		      facti=(GEN)list[i];
		      if(valp((GEN)facti[2])&&lgef(facti)<=5)
			prod=gmul(prod,facti);
		    }
		  if(lgef(prod)>5) 
		    {
		      printf("bug factorpadicnonun");fflush(stdout);exit(1);
		    }
		  e0=truecoeff(prod,2);e1=truecoeff(prod,1);
		  e2=truecoeff(prod,0);
		  dism=valp(gsub(gsqr(e1),gmul2n(gmul(e0,e2),2)))-1;
		}
	      condp=dismin-dism-3;
	      printf("[II-II*{%ld}] page 176, H{%ld}, f=%ld\n",dism,dism+1,condp);fflush(stdout);
	    }
	    break;
	  case 21: 
	    {
	      vb6=myval(truecoeff(polh,0),p);
	      if(vb6<2) {printf("bug red3\n");fflush(stdout);exit(1);}
	      condp=dismin-14;
	      printf("[IV*-II{0}] page 175, (1), f=%ld\n",condp);fflush(stdout);
	    }
	    break;
	  case 30: 
	    { 
	      vb5=myval(truecoeff(polh,1),p); 
	      if(vb5==2) 
		{
		  if(tt>=5) {printf("bug sur tt 6\n");fflush(stdout);exit(1);}
		  goto modere;
		}
	      condp=dismin-7;
	      printf("[II*-III-(-1)] page 167, (2), f=%ld\n",condp);fflush(stdout);
	    }
	    break;
	  }
      }
      break;
    case 2:
      {
	if(!cmpis(denom(theta),4)) 
	  {
	    if(tt>4) {printf("bug sur tt 5\n");fflush(stdout);exit(1);}
	    goto modere;
	  }
	if(cmpis(p,3)&&!cmpis(denom(theta),3)) goto modere;
	list=factorpadicnonun(polh,p,dismin-10*alpha);
	nb=lg(list);prod=polun[varn(polh)];
	for(i=1;i<nb;i++)
	  {
	    facti=(GEN)list[i];
	    if(!valp((GEN)facti[2])) 
	      prod=gmul(prod,facti);
	  }
	if(lgef(prod)>5) 
	  {
	    printf("bug factorpadicnonun 2");
	    fflush(stdout);exit(1);
	  }
	e0=truecoeff(prod,2);e1=truecoeff(prod,1);e2=truecoeff(prod,0);
	dism=valp(gsub(gsqr(e1),gmul2n(gmul(e0,e2),2)));
	switch(itos(gmulgs(theta,12))+alpha-4)
	  {
	  case 0:
	    condp=dismin-dism-1;
	    printf("[IV-II{%ld}] page 175, (%ld), f=%ld\n",dism,3*dism+2,condp);
	    fflush(stdout);break;
	  case 1: 
	    condp=dismin-dism-10;
	    printf("[II*-II*{%ld}] page 175, H{%ld}, f=%ld\n",dism,dism+1,condp);
	    fflush(stdout);break;
	  case 2: case 3:
	    {
	      if(myval(truecoeff(polh,0),p)==2) 
		{
		  if(tt>4) {printf("bug sur tt 5\n");fflush(stdout);exit(1);}
		  goto modere;
		}
	      else
		{ 
		  dism=dism+1;
		  indice=val[6]-(5*val[3]/2)-dism;
		  if((dism*indice)&1)
		    {
		      condp=dismin-dism-indice-2;
		      printf("[II{%ld-%ld}] page 182, (2)x(%ld), f=%ld\n",
		      dism,indice,2*dism,condp);fflush(stdout);
		    }
		  else 
		    {
		      condp=dismin-dism-indice-2;
		      printf("[II{%ld-%ld}] page 182, (%ld), f=%ld\n",
		      dism,indice,4*dism,condp);fflush(stdout);
		    }
		}
	    }
	    break;
	  case 4:  
	    condp=dismin-dism-5;
	    printf("[IV*-II{%ld}] page 175, (%ld), f=%ld\n",dism+1,3*dism+4,condp);
	    fflush(stdout);break;
	  }
      }
      break;
    case 3:
      {
	if(cmpis(p,3)||tt<=4) goto modere;
	goto labelm3;
      }
      break;
    default: printf("bug switch sur lambda\n");fflush(stdout);exit(1);	   
    }
  if(condp<2||condp>maxc) 
    {printf("bug conducteur 3\n");fflush(stdout);exit(1);}
  goto recp;

 labelm3:
  polh1=gcopy(polh);
  theta1=gcopy(theta);
  alpha1=alpha;
  polh=gmul(gsubst(polh,0,ginv(polx[0])),gpuigs(polx[0],6));
  polf=polymini(gmul(polh,gpuigs(p,alpha)),p);
  polh=(GEN)polf[1];
  lambda=itos((GEN)polf[2]);  
  theta=(GEN)polf[3];
  alpha=itos((GEN)polf[4]);
  beta=itos((GEN)polf[6]);
  if(!lambda==3) 
    {printf("bug lambda=3\n");fflush(stdout);exit(1);}
  R=beta-alpha1-alpha;
  if(R&1) 
    {
      printf("bug R impair\n");fflush(stdout);exit(1);
    }
  R>>=1;
  if(R<=-2) 
    {
      printf("bug R <=-2\n");fflush(stdout);exit(1);
    }
  if(val[eps2]%(2*eps)) 
    {
      printf("bug sur val[eps2]\n");fflush(stdout);exit(1);
    }
  if(R>=0&&(alpha+alpha1)>=1) 
    {
      printf("bug equation minimale\n");fflush(stdout);exit(1);
    }
  r1=itos(gmulgs(theta1,6))+6*alpha1; 
  r2=itos(gmulgs(theta,6))+6*alpha; 
 
 litredtp: 
  if((r1==0||r1==6)&&(r2==0||r2==6))
    {
      if(tt==5)
	{      
	  switch(2*r2+r1)
	    {
	    case 0: 
	      condp=0;
	      printf("[I{0}-I{0}-%ld] page 158, (1), f=0\n",R);
	      fflush(stdout);break;
	    case 6: case 12:
	      condp=2;
	      printf("[I*{0}-I{0}-%ld] page 159, (2)^2, f=2\n",R);
	      fflush(stdout);break;
	    case 18: 
	      condp=4;
	      printf("[I*{0}-I*{0}-%ld] page 158, (2)^4, f=4\n",R);
	      fflush(stdout);break;
	    }
	  goto recp;
	}
      if(r1==r2) goto modere;
      if(tt==6) 
	{
	  d=val[6]-val[7]+(val[eps2]/eps);
	  if(r1&&alpha1==0) 
	    {
	      polh1=gdiv(gsubst(polh1,0,gmul(p,polx[0])),gpuigs(p,3));
	    }
	  polhp=gmul(unmodp,polh1);	  
	  if(!gcmp0(discsr(polhp))) 
	    {
	      indice=0; condp=3-r2/6;
	    }
	  else 
	    {
	      indice=d; condp=3-r1/6;
	    }
	}
      else /* donc tt==7 */ 
	{
          if(gcmp1(theta1)) 
	    {
	      polh1=gdiv(gsubst(polh1,0,gmul(p,polx[0])),gpuigs(p,3));
	    }
	  d=val[6]-3*val[3]+(val[eps2]/eps);
	  d1=min(val[7]-3*val[3],d/2);
	  if(d==2*d1) indice=d1;
	  else
	    {
	      indice=discpart(polh1,p,d1+1); 	  
	      if(indice>=d1+1) indice=d-d1;
	      else indice=d1;
	    }
	  condp=3;
	}
      if(r1)
	{
	  if(tt==6)
	    {
	      printf("[I*{%ld}-I{%ld}-%ld] page 170, H{%ld}x(%ld), f=%ld\n",
		     indice,d-indice,R,indice,d-indice,condp);
	      fflush(stdout);goto recp;
	    }
	  else
	    {
	      printf("[I*{%ld}-I{%ld}-%ld] page 180, H{%ld}x(%ld), f=%ld\n",
		     indice,d-indice,R,indice,d-indice,condp);
	      fflush(stdout);goto recp;
	    }
	}
      else
	{
	  if(tt==6)
	    {
	      printf("[I{%ld}-I*{%ld}-%ld] page 170, (%ld)xH{%ld}, f=%ld\n",
		     indice,d-indice,R,indice,d-indice,condp);
	      fflush(stdout);goto recp;
	    }
	  printf("[I{%ld}-I*{%ld}-%ld] page 180, (%ld)xH{%ld}, f=%ld\n",
		 indice,d-indice,R,indice,d-indice,condp);
	  fflush(stdout);goto recp;
	}
    }
  if(tt==7) 
    {printf("bug avant swith sur ri\n");fflush(stdout);exit(1);}
  switch(r1) 
    {
    case 0: case 6:
      { 
	if(r1&&alpha1==0) 
	  {
	    polh1=gdiv(gsubst(polh1,0,gmul(p,polx[0])),gpuigs(p, 3));
	  }
	polhp=gmul(unmodp,polh1);	  	
	if(!gcmp0(discsr(polhp))) d=0;
	else d=val[6]-val[7]+(val[eps2]/eps);
	if(r1) 
	  {printf("[I*{%ld}-",d);comp=d+5;}
	else 
	  {
	    printf("[I{%ld}-",d);
	    if(d) comp=d;
	    else comp=1;
	  }
      }
      fflush(stdout);break;
    case 3: printf("[III-");fflush(stdout);comp=2;break;
    case 9: printf("[III*-");fflush(stdout);comp=8;break;
    case 2: printf("[II-");fflush(stdout);comp=1;break;
    case 8: printf("[IV*-");fflush(stdout);comp=7;break;
    case 4: printf("[IV-");fflush(stdout);comp=3;break;
    case 10: printf("[II*-");fflush(stdout);comp=9;break;
    default: printf("bug type1\n");fflush(stdout);exit(1);
    }
  switch(r2)  
    { 
    case 0: case 6:
      {
	if(r2&&alpha==0)
	  {
	    polh=gdiv(gsubst(polh,0,gmul(p,polx[0])),gpuigs(p,3));
	  }
	polhp=gmul(unmodp,polh);	  
	if(!gcmp0(discsr(polhp))) indice=0;
	else indice=val[6]-val[7]+(val[eps2]/eps);
	if(r2) 
	  {printf("I*{%ld}-%ld] pages 159-177, ",indice,R);comp+=indice+5;}
	else 
	  {
	    printf("I{%ld}-%ld] pages 159-177, ",indice,R);
	    if(indice) comp+=indice;
	    else comp+=1;
	  }
      }
      break;
    case 3: printf("III-%ld] pages 161-177, ",R);comp+=2;break;
    case 9: printf("III*-%ld] pages 162-177, ",R);comp+=8;break;
    case 2: printf("II-%ld] pages 159-174, ",R);comp+=1;break;
    case 8: printf("IV*-%ld] pages 160-175, ",R);comp+=7;break;
    case 4: printf("IV-%ld] pages 160-174, ",R);comp+=3;break;
    case 10: printf("II*-%ld] pages 160-174, ",R);comp+=9;break;
    default: printf("bug type2\n");fflush(stdout);exit(1);
    }
  if(R>=0) condp=dismin-comp+2-12*R;
  else condp=dismin-comp+4;
  if(condp>maxc) 
    {printf("bug conducteur\n");fflush(stdout);exit(1);}
  switch(r1) 
    {
    case 0: 
      if(indice) printf("(%ld)x",d);
      else printf("(1)x");
      break;
    case 6: printf("H{%ld}x",d);break;
    case 3: printf("(2)x");break;
    case 9: printf("(2)x");break;
    case 2: printf("(1)x");break;
    case 8: printf("(3)x");break;
    case 4: printf("(3)x");break;
    case 10: printf("(1)x");break;
    default: printf("bug type3\n");fflush(stdout);exit(1);
    }
  switch(r2) 
    {
    case 0: 
      if(indice) 
	{printf("(%ld), f=%ld\n",indice,condp);fflush(stdout);}
      else 
	{printf("(1), f=%ld\n",condp);fflush(stdout);}
      break;
    case 6: printf("H{%ld}, f=%ld\n",indice,condp);fflush(stdout);break;
    case 3: printf("(2), f=%ld\n",condp);fflush(stdout);break;
    case 9: printf("(2), f=%ld\n",condp);fflush(stdout);break;
    case 2: printf("(1), f=%ld\n",condp);fflush(stdout);break;
    case 8: printf("(3), f=%ld\n",condp);fflush(stdout);break;
    case 4: printf("(3), f=%ld\n",condp);fflush(stdout);break;
    case 10: printf("(1), f=%ld\n",condp);fflush(stdout);break;
    default: printf("bug type4\n");fflush(stdout);exit(1);
    }
  goto recp;

 quadratic:
  if(cmpis(p,3)) goto modere;
  alpha1=alpha;
  polf=(GEN)polyminizi(gmul(polh,gpuigs(p,alpha)));
  theta=(GEN)polf[1];
  alpha=itos((GEN)polf[2]); 
  beta=itos((GEN)polf[3]); 
  if(alpha&&beta>=1) 
    {printf("bug, erreur d'appreciation\n");fflush(stdout);exit(1);}
  R=beta-alpha;
  if(R>=0&&alpha1) 
    { 
      dismin=dismin-10;
      printf("(care, the minimal discriminant over Z[i] is smaller than over Z) ");fflush(stdout);
    }
  r1=itos(gmulgs(theta,6))+6*alpha;
  r2=r1;
  alpha1=alpha;
  theta1=gcopy(theta);
  goto litredtp;
  
 quartic:
  polf=(GEN)polyminizi2(gmul(polh,gpuigs(p,alpha)));
  theta=(GEN)polf[1];
  beta=itos((GEN)polf[2]);
  if(beta&1)
    {
      printf("bug, le type sur Z[i] doit etre [K-K-(2*m)]\n");
      fflush(stdout);exit(1);
    }
  R=beta/2;
  r1=itos(gmulgs(theta,6));
  switch(tt)
    {
    case 1: case 5: d=0;break;
    case 3: d=val[6]-5*val[3]/2;break;
    case 7: d=val[6]-3*val[3]+val[eps2]/eps;break;
    default: printf("bug choix de types\n");fflush(stdout);exit(1);
    }
  switch(r1) 
    {
    case 0: 
      {
	if(d) 
	  {
	    condp=3;printf("[2I{%ld}-%ld] page 181, (%ld), f=3\n",d,R,d);
	  }
	else 
	  {
	    condp=2;
	    if(R)
	      {
		printf("[2I{0}-%ld] page 159, (1), f=2\n",R);
	      }
	    else
	      {
		printf("[II] page 155, (1), f=2\n");
	      }
	  }
      }
      break;
    case 6: 
      {
	condp=4;printf("[2I*{%ld}-%ld] pages 159, 181, (2)^2, f=4\n",d,R);
      }
      break;
    case 3: 
      condp=4;printf("[2III-%ld] page 168, (2), f=4\n",R);break;
    case 9: 
      condp=4;printf("[2III*-%ld] page 168, (2), f=4\n",R);break;
    case 2: 
      condp=dismin-12*R-13;printf("[2II-%ld] page 162, (1), f=%ld\n",R,condp);break;
    case 8: 
      condp=dismin-12*R-19;printf("[2IV*-%ld] page 165, (3), f=%ld\n",R,condp);break;
    case 4: 
      condp=dismin-12*R-15;printf("[2IV-%ld] page 165, (3), f=%ld\n",R,condp);break;
    case 10: 
      condp=dismin-12*R-21;printf("[2II*-%ld] page 163, (1), f=%ld\n",R,condp);break;
    default: printf("bug type1\n");fflush(stdout);exit(1);
    }
  fflush(stdout);
  if(condp>maxc||condp<0) 
    {printf("bug conducteur\n");fflush(stdout);exit(1);}
  goto recp;

 modere:
/*  printf("(moderee) ");fflush(stdout);  */
  apol2=lisseq("a0;a1;a2;a3;a4;a5;a6;-5*a1^2+12*a0*a2");
  apol3=lisseq("a0;a1;a2;a3;a4;a5;a6;5*a1^3+9*a0*(-2*a1*a2+3*a0*a3)");
  apol4=lisseq("a0;a1;a2;a3;a4;a5;a6;-5*a1^4+24*a0*(a1^2*a2-3*a0*a1*a3+6*a0^2*a4)");
  apol5=lisseq("a0;a1;a2;a3;a4;a5;a6;a1^5+3*a0*(-2*a1^3*a2+9*a0*a1^2*a3-36*a0^2*a1*a4+108*a0^3*a5)");
  bpol2=lisseq("a0;a1;a2;a3;a4;a5;a6;2*a2^2-5*a1*a3+10*a0*a4");
  A2=gsubst(gsubst(gsubst(gsubst(gsubst(gsubst(gsubst(apol2,1,a0),2,a1),3,a2),4,a3),5,a4),6,a5),7,a6);
  A3=gsubst(gsubst(gsubst(gsubst(gsubst(gsubst(gsubst(apol3,1,a0),2,a1),3,a2),4,a3),5,a4),6,a5),7,a6);
  A4=gsubst(gsubst(gsubst(gsubst(gsubst(gsubst(gsubst(apol4,1,a0),2,a1),3,a2),4,a3),5,a4),6,a5),7,a6);
  A5=gsubst(gsubst(gsubst(gsubst(gsubst(gsubst(gsubst(apol5,1,a0),2,a1),3,a2),4,a3),5,a4),6,a5),7,a6);
  B2=gsubst(gsubst(gsubst(gsubst(gsubst(gsubst(gsubst(bpol2,1,a0),2,a1),3,a2),4,a3),5,a4),6,a5),7,a6);
  va0=myval(a0,p);va2=myval(A2,p);va3=myval(A3,p);va4=myval(A4,p);
  va5=myval(A5,p);vb2=myval(B2,p); 

  switch(tt)
    {
    case 1:
      if((!gcmp0(A5))&&(20*va0+val[6]>6*va5))
	{
	  n=glcm(denom(pro1=gdiv(stoi(val[6]-2*va5),stoi(20))),denom(pro2=gdiv(stoi(5*val[6]-6*va5),stoi(40))));
	  r=gmul(n,pro1);q=gmul(n,pro2);
	}
      else
	{
	  n=glcm(denom(pro1=gdiv(stoi(10*va0-val[6]),stoi(30))),denom(pro2=gdiv(stoi(5*va0-val[6]),stoi(10))));
	  r=gmul(n,pro1);q=gmul(n,pro2);
	}
      r=gmod(r,n);q=gmod(q,n);
      switch(itos(n))
	{
	case 1: condp=0;
	  printf("[I{0-0-0}] page 155, (1)");fflush(stdout);break;
	case 2:
	  switch(itos(r))
	    {
	    case 0: condp=4;
	      printf("[I*{0-0-0}] page 155, (2)^4");fflush(stdout);break;
	    case 1: condp=2;
	      printf("[II] page 155, (1)");fflush(stdout);break;
	    default: printf("bug1\n");fflush(stdout);exit(1);
	    }
	  break;
	case 4: condp=4;printf("[VI] page 156, (2)^2");fflush(stdout);break; 
	default: printf("bug8\n");fflush(stdout);exit(1);	  
	}
      break;
    case 2: case 3: case 4:
      switch(tt)
	{
	case 2: j12=i12;break;
	case 3: j12=gpuigs(i4,3);break;
	case 4: j12=gpuigs(j2,6);break;
	}
      v12=myval(j12,p);
      if((9*vb2>=(6*va0+v12))&&(36*va5>=(120*va0+5*v12)))
	{
	  n=glcm(denom(pro1=gdiv(stoi(12*va0-v12),stoi(36))),denom(pro2=gdiv(stoi(6*va0-v12),stoi(12))));
	  r=gmul(n,pro1);q=gmul(n,pro2);flc=1;
	}
      else
	{
	  if(((120*va0+5*v12)>36*va5)&&(60*vb2>=(12*va5+5*v12)))
	    {
	      n=denom(pro1=gdiv(stoi(36*va5-25*v12),stoi(240)));
	      q=gmul(n,pro1);r=gmulsg(-2,q);flc=1;
	    }
	  else
	    {
	      if(((6*va0+v12)>9*vb2)&&((12*va5+5*v12)>60*vb2))
		{
		  n=glcm(denom(pro1=gdiv(stoi(v12-6*vb2),stoi(12))),denom(pro2=gdiv(stoi(v12-9*vb2),stoi(12))));
		  r=gmul(n,pro1);q=gmul(n,pro2);flc=2;
		}
	      else {printf("bug9\n");fflush(stdout);exit(1);}
	    }
	}
      r=gmod(r,n);q=gmod(q,n);
      switch(tt)
	{
	case 2:
	  d=itos(gmul(n,gdivgs(stoi(6*val[6]-5*val[7]),6)));
	  switch(itos(n))
	    {
	    case 1: condp=1;
	      printf("[I{%ld-0-0}] page 170, (%ld)",d,d);fflush(stdout);break;	  
	    case 2:
	      switch(itos(r))
		{
		case 0: condp=4;
		  printf("[I*{%ld-0-0}] page 171, (2)^2xH{%ld}",d/2,d/2);
		  fflush(stdout);break;	  		  
		case 1:
		  switch(itos(q))
		    {
		    case 0: condp=2;
		      printf("[II*{%ld-0}] page 172, (1)",d/2);
		      fflush(stdout);break;
		    case 1: condp=3;
		      printf("[II{%ld-0}] page 171, (%ld)",d/2,2*d);
		      fflush(stdout);break;
		    default: {printf("bug10\n");fflush(stdout);exit(1);}
		    }
		  break;
		default: {printf("bug11\n");fflush(stdout);exit(1);}
		}
	      break;
	    case 3:
	      switch(itos(r))
		{
		case 1: condp=3;
		  printf("[IV-II{%ld}] page 175, (%ld)",(d-2)/3,d);
		  fflush(stdout);break;		  
		case 2: condp=3;
		  printf("[IV*-II{%ld}] page 175, (%ld)",(d-1)/3,d);
		  fflush(stdout);break;
		default: {printf("bug12\n");fflush(stdout);exit(1);}
		}
	      break;
	    case 4:
	      switch(itos(r))
		{
		case 1:
		  switch(itos(q))
		    {
		    case 1: condp=3;
		      printf("[III-II{%ld}] page 177, (%ld)",(d-2)/4,d/2);
		      fflush(stdout);break;
		    case 3: condp=4;
		      printf("[III*-II*{%ld}] page 178, (8)",(d-2)/4);
		      fflush(stdout);break;
		    default: {printf("bug13\n");fflush(stdout);exit(1);}
		    }
		  break;
		case 3:
		  switch(itos(q))
		    {
		    case 1: condp=4;
		      printf("[III-II*{%ld}] page 178, (8)",(d-2)/4);
		      fflush(stdout);break;
		    case 3: condp=3;
		      printf("[III*-II{%ld}] page 178, (%ld)",(d-2)/4,d/2);
		      fflush(stdout);break;
		    default: {printf("bug14\n");fflush(stdout);exit(1);}
		    }
		  break;
		default: {printf("bug15\n");fflush(stdout);exit(1);}
		}
	      break;
	    case 6:
	      switch(itos(r))
		{
		case 2: condp=4;
		  printf("[II*-II*{%ld}] page 176, H{%ld}",(d-4)/6,(d+2)/6);
		  fflush(stdout);break;		  
		case 4: condp=4;
		  printf("[II-II*{%ld}] page 176, H{%ld}",(d-2)/6,(d+4)/6);
		  fflush(stdout);break;
		default: {printf("bug16\n");fflush(stdout);exit(1);}
		}
	      break;
	    default: {printf("bug17\n");fflush(stdout);exit(1);}
	    }
	  break;
	case 3:
	  va5=2*val[6]-5*val[3];e1=gmin(stoi(val[7]-3*val[3]),gmul2n(stoi(va5),-2));
	  e2=gsub(gmul2n(stoi(va5),-1),e1);
	  d1=itos(gmul(n,e1));d2=itos(gmul(n,e2));
	  switch(itos(n))
	    {
	    case 1: condp=2;
	      printf("[I{%ld-%ld-0}] page 179, (%ld)x(%ld)",d1,d2,d1,d2);
	      fflush(stdout);break;	     
	    case 2:
	      switch(itos(r))
		{
		case 0: condp=4;
		  printf("[I*{%ld-%ld-0}] page 180, H{%ld}xH{%ld}",d1/2,d2/2,d1/2,d2/2);
		  fflush(stdout);break;
		case 1:
		  switch(flc)
		    {
		    case 1:condp=3;
		      printf("[2I{%ld}-0] page 181, (%ld)",d1,d1);
		      fflush(stdout);break;		      
		    case 2: condp=3;
		      printf("[II{%ld-%ld}] page 182, ",d1/2,d2/2);
		      if((d1*d2-4)&7) printf("(%ld)",2*d1);
		      else printf("(%ld)x(2)",d1);
                      printf("ou [II{%ld-%ld}] page 182, ",d2/2,d1/2);
		      if((d1*d2-4)&7) printf("(%ld)",2*d2);
		      else printf("(%ld)x(2)",d2);
		      fflush(stdout);break;
		    default: {printf("bug19\n");fflush(stdout);exit(1);}
		    }
		  break;
		default: {printf("bug20\n");fflush(stdout);exit(1);}
		}
	      break;
	    case 4: condp=4;
	      printf("[III{%ld}] page 182, H{%ld}",d1/2,d1/2);
	      fflush(stdout);break;	      
	    default: {printf("bug21\n");fflush(stdout);exit(1);}
	    }
	  break;
	case 4:
	  vl=stoi(val[6]-5*val[1]);vn=stoi(val[7]-6*val[1]);vm=stoi(val[2]-2*val[1]);
	  e1=gmin(gmin(gdivgs(vl,3),gmul2n(vn,-1)),vm);
	  e2=gmin(gmul2n(gsub(vl,e1),-1),gsub(vn,e1));
	  e3=gsub(vl,gadd(e1,e2));
	  d1=itos(gmul(n,e1));d2=itos(gmul(n,e2));d3=itos(gmul(n,e3));
	  g=d1*d2+d1*d3+d2*d3;h=itos(mppgcd(mppgcd(stoi(d1),stoi(d2)),stoi(d3)));
	  switch(itos(n))
	    {
	    case 1: condp=2;
	      printf("[I{%ld-%ld-%ld}] page 182, (%ld)x(%ld)",d1,d2,d3,h,g/h);
	      fflush(stdout);break;	     
	    case 2:
	      switch(itos(r))
		{
		case 0: condp=4; 
		  printf("[I*{%ld-%ld-%ld}] page 183, H{%ld}xH{%ld}",d1/2,d2/2,d3/2,g/4,2-((h&2)>>1));
		  fflush(stdout);break;
		case 1:
		  if(d1==d2) f2=d1;
		  else
		    {
		      if(d1==d3) f2=d1;
		      else
			{
			  if(d2==d3) f2=d2;
			  else {printf("bug23\n");fflush(stdout);exit(1);}
			}
		    }
		  f1=d1+d2+d3-2*f2;
		  switch(itos(q))
		    {
		    case 0: condp=3;
		      printf("[II*{%ld-%ld}] page 184, (%ld)",f1/2,f2,f2);
		      fflush(stdout);break;		      
		    case 1: condp=3;
		      printf("[II{%ld-%ld}] page 183, (%ld)",f1/2,f2,2*f1+f2);
		      fflush(stdout);break;
		    default: {printf("bug24\n");fflush(stdout);exit(1);}
		    }
		  break;
		default: {printf("bug25\n");fflush(stdout);exit(1);}
		}
	      break;
	    case 3: condp=4;
	      printf("[III{%ld}] page 184, ",d1);
	      if(d1%3) printf("(9)");else printf("(3)^2");
	      fflush(stdout);break;	      
	    case 6: condp=4;
	      printf("[III*{%ld}] page 184, (1)",d1/2);
	      fflush(stdout);break;	      
	    default: {printf("bug26\n");fflush(stdout);exit(1);}
	    }
	  break;
	}
      break;
    case 5: case 6: case 7:
      switch(tt)
	{
	case 5: dk=gdivgs(stoi(eps*val[6]-5*val[eps2]),12*eps);break;
	case 6: dk=gdivgs(stoi(eps*val[7]-6*val[eps2]),12*eps);
	  d1k=gdivgs(stoi(eps*val[6]+val[eps2]-eps*val[7]),eps);break;
	case 7: dk=gdivgs(stoi(eps*val[3]-2*val[eps2]),4*eps);
	  pro1=gdivgs(stoi(eps*val[6]+val[eps2]-3*eps*val[3]),eps);
	  d1k=gmin(stoi(val[7]-3*val[3]),gmul2n(pro1,-1));
	  d2k=gsub(pro1,d1k);break;
	}
      rk=gadd(gmul2n(stoi(va0),-1),gmin(gmul2n(dk,-1),gmin(gmul2n(stoi(2*va3-3*va2),-3),gdivgs(stoi(2*myval(gsub(gmul(A2,A3),gmulsg(3,A5)),p)-5*va2),12))));
      v1=2*va3-4*va0-val[1];
      v2=6*va5-20*va0-5*val[1];
/* la definition de n est differente suivant val[1] mod 2, ici elle n'est pas 
   valable pour p=2 ou 3 */
      if(!(val[eps2]&1))
	{
	  if((3*vb2>=(2*va0+2*val[1]))&&(v1>=0)&&(v2>=0)&&((v1==0)||(v2==0)))
	    {
	      n=glcm(denom(dk),denom(pro1=gdivgs(stoi(va0+val[1]),6)));
	      r=gmul(n,pro1);
	    }
	  else
	    {
	      if(((20*va0+5*val[1])>6*va5)&&(10*vb2>=(2*va5+5*val[1])))
		{
		  n=glcm(denom(dk),denom(pro1=gdivgs(stoi(2*va5+val[1]),8)));
		  r=gmul(n,pro1);
		}
	      else
		{
		  if(((2*va0+2*val[1])>3*vb2)&&((2*va5+5*val[1])>10*vb2))
		    {
		      n=glcm(denom(dk),denom(pro1=gmul2n(stoi(vb2),-2)));
		      r=gmul(n,pro1);
		    }
		  if((3*vb2>=(2*va0+2*val[1]))&&(2*va3>(4*va0+val[1]))&&(6*va5>(20*va0+5*val[1])))
		    {
		      if(gcmp0(A2)) {printf("bug27\n");fflush(stdout);exit(1);}
		      n=glcm(denom(dk),denom(rk));r=gmul(n,rk);
		    }
		  else 
		    {
		      if(!cmpis(p,3)) /* following M. Stoll */
			{
			  printf("bug28\n");fflush(stdout);exit(1);
			}
		    }
		}
	    }
	}
      else
	{
	  m=denom(dk);r=gmul(m,dk);n=gmul2n(m,1);
	}
      if(!cmpis(p,3)) n=stoi(1+r1/6);  
      d=itos(gmul(n,dk));dm=gmod(stoi(d),n);r=gmod(r,n);
      if(!cmpis(p,3)) dm=gzero;    
      switch(tt)
	{
	case 5:
	  if(!(val[eps2]&1))
	    {
	      switch(itos(n))
		{
		case 1: condp=0;
		  printf("[I{0}-I{0}-%ld] page 158, (1)",d);
		  fflush(stdout);break;
		case 2:
		  switch(itos(dm))
		    {
		    case 0: condp=4;
		      printf("[I*{0}-I*{0}-%ld] page 158, (2)^4",(d-2)/2);
		      fflush(stdout);break;
		    case 1: condp=2;
		      printf("[I{0}-I*{0}-%ld] page 159, (2)^2",(d-1)/2);
		      fflush(stdout);break;
		    }
		  break;
		case 3:
		  switch(itos(dm))
		    {
		    case 0: condp=4;
		      printf("[IV-IV*-%ld] page 165, (3)^2",(d-3)/3);
		      fflush(stdout);break;
		    case 1:
		      switch(itos(r))
		    {
		    case 0: case 1: condp=2;
		      printf("[I{0}-IV-%ld] page 160, (3)",(d-1)/3);
		      fflush(stdout);break;
		    case 2: condp=4;
		      printf("[IV*-IV*-%ld] page 166, (3)^2",(d-4)/3);
		      fflush(stdout);break;
		    }
		      break;
		    case 2:
		      switch(itos(r))
			{
			case 0: case 2: condp=2;
			  printf("[I{0}-IV*-%ld] page 160, (3)",(d-2)/3);
			  fflush(stdout);break;
			case 1: condp=4;
			  printf("[IV-IV-%ld] page 165, (3)^2",(d-2)/3);
			  fflush(stdout);break;
			}
		      break;
		    }
		  break;	
		case 4:
		  switch(itos(dm))
		    {
		    case 0: condp=4;
		      printf("[III-III*-%ld] page 169, (2)^2",(d-4)/4);
		      fflush(stdout);break;
		    case 1:
		      switch(itos(r))
			{
			case 0: case 1: condp=2;
			  printf("[I{0}-III-%ld] page 161, (2)",(d-1)/4);
			  fflush(stdout);break;
			case 2: case 3: condp=4;
			  printf("[I*{0}-III*-%ld] page 162, (2)^3",(d-5)/4);
			  fflush(stdout);break;
			}
		      break;
		    case 2:
		      switch(itos(r))
			{
			case 1: condp=4;
			  printf("[III-III-%ld] page 169, (2)^2",(d-2)/4);
			  fflush(stdout);break;
			case 3: condp=4;
			  printf("[III*-III*-%ld] page 169, (2)^2",(d-6)/4);
			  fflush(stdout);break;
			default: {printf("bug29\n");fflush(stdout);exit(1);}
			}
		      break;
		    case 3:		    
		      switch(itos(r))
			{
			case 0: case 3: condp=2;
			  printf("[I{0}-III*-%ld] page 162, (2)",(d-3)/4);
			  fflush(stdout);break;
			case 1: case 2: condp=4;
			  printf("[I*{0}-III-%ld] page 162, (2)^3",(d-3)/4);
			  fflush(stdout);break;
			}
		      break;
		    }
		  break;	    
		case 6:
		  switch(itos(dm))
		    {
		    case 0: condp=4;
		      printf("[II-II*-%ld] page 163, (1)",(d-6)/6);
		      fflush(stdout);break;
		    case 1:
		      switch(itos(r))
			{
			case 0: case 1: condp=2;
			  printf("[I{0}-II-%ld] page 159, (1)",(d-1)/6);
			  fflush(stdout);break;
			case 2: case 5: condp=4;
			  printf("[II*-IV-%ld] page 164, (3)",(d-7)/6);
			  fflush(stdout);break;
			case 3: case 4: condp=4;
			  printf("[I*{0}-IV*-%ld] page 161, (2)^2x(3)",(d-7)/6);
			  fflush(stdout);break;
			}
		      break;
		    case 2:
		      switch(itos(r))
			{
			case 1: condp=4;
			  printf("[II-II-%ld] page 163, (1)",(d-2)/6);
			  fflush(stdout);break;
			case 3: case 5: condp=4;
			  printf("[I*{0}-II*-%ld] page 160-161, (2)^2",(d-8)/6);
			  fflush(stdout);break;
			default: {printf("bug30\n");fflush(stdout);exit(1);}
			}
		      break;
		    case 3:
		      switch(itos(r))
			{
			case 1: case 2: condp=4;
			  printf("[II-IV-%ld] page 164, (3)",(d-3)/6);
			  fflush(stdout);break;
			case 4: case 5: condp=4;
			  printf("[II*-IV*-%ld] page 164-165, (3)",(d-9)/6);
			  fflush(stdout);break;
			default: {printf("bug31\n");fflush(stdout);exit(1);}
			}
		      break;
		    case 4:
		      switch(itos(r))
			{
			case 1: case 3: condp=4;
			  printf("[I*{0}-II-%ld] page 160, (2)^2",(d-4)/6);
			  fflush(stdout);break;
			case 5: condp=4;
			  printf("[II*-II*-%ld] page 163, (1)",(d-10)/6);
			  fflush(stdout);break;
			default: {printf("bug32\n");fflush(stdout);exit(1);}
			}
		      break;
		    case 5:
		      switch(itos(r))
			{
			case 0: case 5: condp=2;
			  printf("[I{0}-II*-%ld] page 160, (1)",(d-5)/6);
			  fflush(stdout);break;
			case 1: case 4: condp=4;
			  printf("[II-IV*-%ld] page 164, (3)",(d-5)/6);
			  fflush(stdout);break;
			case 2: case 3: condp=4;
			  printf("[I*{0}-IV-%ld] page 161, (2)^2x(3)",(d-5)/6);
			  fflush(stdout);break;
			}
		      break;
		    default: {printf("bug33\n");fflush(stdout);exit(1);}
		    }
		  break;
		case 12:
		  switch(itos(dm))
		    {
		    case 1:
		      switch(itos(r))
			{
			case 3: case 10: condp=4;
			  printf("[II*-III-%ld] page 166-167, (2)",(d-13)/12);
			  fflush(stdout);break;
			case 4: case 9: condp=4;
			  printf("[IV-III*-%ld] page 167, (6)",(d-13)/12);
			  fflush(stdout);break;
			default: {printf("bug34\n");fflush(stdout);exit(1);}
			}
		      break;
		    case 5:
		      switch(itos(r))
			{
			case 2: case 3: condp=4;
			  printf("[II-III-%ld] page 166, (2)",(d-5)/12);
			  fflush(stdout);break;
			case 8: case 9: condp=4;
			  printf("[IV*-III*-%ld] page 168, (6)",(d-17)/12);
			  fflush(stdout);break;
			default: {printf("bug35\n");fflush(stdout);exit(1);}
			}
		      break;
		    case 7:
		      switch(itos(r))
			{
			case 3: case 4: condp=4;
			  printf("[IV-III-%ld] page 167, (6)",(d-7)/12);
			  fflush(stdout);break;
			case 9: case 10: condp=4;
			  printf("[II*-III*-%ld] page 167, (2)",(d-19)/12);
			  fflush(stdout);break;
			default: {printf("bug36\n");fflush(stdout);exit(1);}
			}
		      break;
		    case 11:
		      switch(itos(r))
			{
			case 3: case 8: condp=4;
			  printf("[IV*-III-%ld] page 168, (6)",(d-11)/12);
			  fflush(stdout);break;
			case 2: case 9: condp=4;
			  printf("[II-III*-%ld] page 166, (2)",(d-11)/12);
			  fflush(stdout);break;
			default: {printf("bug37\n");fflush(stdout);exit(1);}
			}
		      break;
		    default: {printf("bug38\n");fflush(stdout);exit(1);}
		    }
		  break;
		default: {printf("bug39\n");fflush(stdout);exit(1);}
		}
	    }
	  else
	    {
	      r=gmod(r,gmul2n(n,-1));
	      switch(itos(n))
		{
		case 2: condp=2;
		  printf("[2I{0}-%ld] page 159, (1)",(d/2));
		  fflush(stdout);break;
		case 4: condp=4;
		  printf("[2I*{0}-%ld] page 159, (2)^2",(d/2-1)/2);
		  fflush(stdout);break;
		case 6:
		  switch(itos(r))
		    {
		    case 1: condp=4;
		      printf("[2IV-%ld] page 165, (3)",(d/2-1)/3);
		      fflush(stdout);break;
		    case 2: condp=4;
		      printf("[2IV*-%ld] page 165, (3)",(d/2-2)/3);
		      fflush(stdout);break;
		    default: {printf("bug40\n");fflush(stdout);exit(1);}
		    }
		  break;
		case 8:
		  switch(itos(r))
		    {
		    case 1: condp=4;
		      printf("[2III-%ld] page 168, (2)",(d/2-1)/4);
		      fflush(stdout);break;
		    case 3: condp=4;
		      printf("[2III*-%ld] page 168, (2)",(d/2-3)/4);
		      fflush(stdout);break;
		    default: {printf("bug41\n");fflush(stdout);exit(1);}
		    }
		  break;
		case 12:
		  switch(itos(r))
		    {
		    case 1: condp=4;
		      printf("[2II-%ld] page 162, (1)",(d/2-1)/6);
		      fflush(stdout);break;
		    case 5: condp=4;
		      printf("[2II*-%ld] page 163, (1)",(d/2-5)/6);
		      fflush(stdout);break;
		    default: {printf("bug42\n");fflush(stdout);exit(1);}
		    }
		  break;
		default: {printf("bug43\n");fflush(stdout);exit(1);}
		}
	    }
	  break;
       	case 6:
	  d1=itos(gmul(n,d1k));
	  switch(itos(n))
	    {
	    case 1: condp=1;
              printf("[I{%ld}-I{0}-%ld] page 170, (%ld)",d1,d,d1);
              fflush(stdout);break;
            case 2:
              switch(itos(dm))
		{
                case 0: condp=4;
                  printf("[I*{0}-I*{%ld}-%ld] page 171, (2)^2xH{%ld}", d1/2,(d-2)/2,d1/2);
                  fflush(stdout);break;
                case 1: goto labelm3;
		default: {printf("bug44\n");fflush(stdout);exit(1);}
		}
              break;
            case 3:
              switch(itos(dm))
		{
                case 1: condp=3;
                  printf("[IV-I{%ld}-%ld] page 173, (3)x(%ld)",d1/3,(d-1)/3,d1/3);
                  fflush(stdout);break;
                case 2: condp=3;
                  printf("[IV*-I{%ld}-%ld] page 173, (3)x(%ld)",d1/3,(d-2)/3,d1/3);
                  fflush(stdout);break;
  	        default: {printf("bug45\n");fflush(stdout);exit(1);}
		}
	      break;
            case 4:
              switch(itos(dm))
		{
                case 1:
                  switch(itos(r))
		    {
                    case 0: case 1: condp=3;
                      printf("[III-I{%ld}-%ld] page 176, (2)x(%ld)",d1/4,(d-1)/4,d1/4);
                      fflush(stdout);break;
                    case 2: case 3: condp=4;
                      printf("[III*-I*{%ld}-%ld] page 177, (2)xH{%ld}",d1/4,(d-5)/4,d1/4);
                      fflush(stdout);break;
		    default: {printf("bug46\n");fflush(stdout);exit(1);}
		    }
                  break;
                case 3:
                  switch(itos(r))
		    {
                    case 0: case 3: condp=3;
                      printf("[III*-I{%ld}-%ld] page 176, (2)x(%ld)",d1/4,(d-3)/4,d1/4);
                      fflush(stdout);break;
                    case 1: case 2: condp=4;
                      printf("[III-I*{%ld}-%ld] page 177, (2)xH{%ld}",d1/4,(d-3)/4,d1/4);
                      fflush(stdout);break;
		    default: {printf("bug47\n");fflush(stdout);exit(1);}
		    }
		  break;
		default: {printf("bug48\n");fflush(stdout);exit(1);}
		}
	      break;
            case 6:
              switch(itos(dm))
		{
                case 1:
                  switch(itos(r))
		    {
                    case 0: case 1: condp=3;
                      printf("[II-I{%ld}-%ld] page 172, (%ld)",d1/6,(d-1)/6,d1/6);
                      fflush(stdout);break;
                    case 3: case 4: condp=4;
                      printf("[IV*-I*{%ld}-%ld] page 174-175, (3)xH{%ld}",d1/6,(d-7)/6,d1/6);
                      fflush(stdout);break;
		    default: {printf("bug49\n");fflush(stdout);exit(1);}
		    }
		  break;
                case 2: condp=4;
                  printf("[II*-I*{%ld}-%ld] page 174, H{%ld}",d1/6,(d-8)/6,d1/6);
                  fflush(stdout);break;
                case 4: condp=4;
                  printf("[II-I*{%ld}-%ld] page 173, H{%ld}",d1/6,(d-4)/6,d1/6);
                  fflush(stdout);break;
                case 5:
                  switch(itos(r))
		    {
                    case 0: case 5: condp=3;
                      printf("[II*-I{%ld}-%ld] page 172, (%ld)",d1/6,(d-5)/6,d1/6);
                      fflush(stdout);break;
                    case 2: case 3: condp=4;
                      printf("[IV-I*{%ld}-%ld] page 174, (3)xH{%ld}",d1/6,(d-5)/6,d1/6);
                      fflush(stdout);break;
		    default: {printf("bug50\n");fflush(stdout);exit(1);}
		    }
		  break;
		default: {printf("bug51\n");fflush(stdout);exit(1);}
		}
              break;
            default: {printf("bug52\n");fflush(stdout);exit(1);}
            }
	  break;
        case 7:
          j2m=val[eps2]&1;d1=itos(gmul(n,d1k));d2=itos(gmul(n,d2k));
          switch(itos(n))
	    {
            case 1: condp=2;
              printf("[I{%ld}-I{%ld}-%ld] page 179, (%ld)x(%ld)",d1,d2,d,d1,d2);
              fflush(stdout);break;
            case 2:
	      if(j2m)
                {
		  condp=3;printf("[2I{%ld}-%ld] page 181, (%ld)",d1,d/2,d1);
		  fflush(stdout);
		}
             else
	       {
		 if(itos(dm)==0)
		   {
		     condp=4;
		     printf("[I*{%ld}-I*{%ld}-%ld] page 180, H{%ld}xH{%ld}",
			    d1/2,d2/2,(d-2)/2, d1/2, d2/2);
		     fflush(stdout);
		   }
		 else
		   {
		     if(d1==d2)
		       {
			 condp=3; 
			 printf("[I{%ld}-I*{%ld}-%ld] page 180, (%ld)xH{%ld}",
				d1/2,d1/2,(d-1)/2,d1/2,d1/2);
			 fflush(stdout);
		       }
		     else goto labelm3;
		   }
	       }
	      break;
            case 4: condp=4;
              printf("[2I*{%ld}-%ld] page 181, H{%ld}",d1/2,(d-2)/4,d1/2);
              fflush(stdout);break;
	    default: {printf("bug55\n");fflush(stdout);exit(1);}
	    }
	  break;
        default: {printf("bug56\n");fflush(stdout);exit(1);}	  
	}
      break;   
    default: {printf("bug57\n");fflush(stdout);exit(1);}
    }
  printf(", f=%ld\n",condp);fflush(stdout);
  goto recp;
}

long
polval(GEN pol, GEN p)      /* la plus grande puissance de p divisant pol */
{
  long lx,v,i;

  lx=lgef(pol);
  if(!signe(pol)) return EXP220;
  else
  {
    v=myval((GEN)pol[2],p);
    for(i=3;i<lx;i++) v=min(myval((GEN)pol[i],p),v);
  }
  return v;
}

/* Factorisation p-adique (sur Q_p) a l'ordre r d'un polynome pol, de 
contenu non divisible par p et sans racine multiple. Le resultat est
un vecteur. */ 

GEN
factorpadicnonun(GEN pol, GEN p, long r) 
{
  long lx=lgef(pol),vx=varn(pol),i,contpol,nb;
  GEN td,list,ep,fact,upol;
  
  if(gcmp1(td=(GEN)pol[lx-1])) 
    {
      list=(GEN)factorpadic4(pol,p,r)[1];
      return list;
    }
  else
    {
      contpol=polval(pol,p);
      if(contpol) {printf("contenu non trivial, erreur\n");fflush(stdout);exit(1);}
      pol=gdiv(pol,gpuigs(p,contpol));
      r=(lx-4)*ggval(td,p)+r;
      upol=gsubst(gmul(pol,gpuigs(td,lx-4)),vx,gdiv(polx[vx],td));
      fact=gsubst(factorpadic4(upol,p,r),vx,gmul(polx[vx],td));
      list=(GEN)fact[1];ep=(GEN)fact[2];nb=lg(list)-1;
      for(i=1;i<=nb;i++)
	if(!gcmp1((GEN)ep[i])) 
	  {
	    printf("erreur, racine multiple\n");fflush(stdout);exit(1);
	  } 
      for(i=1;i<=nb;i++)
	/* (GEN)list[i] = gdiv((GEN)list[i],gpuigs(p,polval((GEN)list[i],p))); */
	list[i] = (long) gdiv((GEN)list[i],gpuigs(p,polval((GEN)list[i],p))); 
    }
  return list;
}
  
GEN 
polymini(GEN pol, GEN p)   /* polynome minimal dans Z_p, p\ne 2 */
{
  GEN polh,polhp,fac,rac,ent,pent,theta,polf,pro;
  long alpha,i,l,lambda,maxord,beta;

  polf=cgetg(7,18);
 rec1:
  alpha=polval(pol,p);polh=gdiv(pol,gpuigs(p,alpha));
  if(gdivise(truecoeff(polh,6),p)&&gdivise(truecoeff(polh,5),p)&&gdivise(truecoeff(polh,4),p)&&gdivise(truecoeff(polh,3),p))
    {polh=gmul(gpuigs(polx[0],6),gsubst(polh,0,ginv(polx[0])));}
  alpha=alpha&1;      
  beta=0;  
  lambda=0; 
  for(i=1;i<=3;i++)
    {
      if(!gdivise(truecoeff(polh,6-i),p)) lambda=i;
    }
   
 labelm6:
  theta=caltheta(polh,p,lambda);
  polf[5]=zero;          /* 1 si racine d'ordre 3 non dans F_p, 0 sinon */
  if(gcmp(theta,gun)>=0)
    { 
      ent=gfloor(theta);pent=gpui(p,ent,0);
      polh=gdiv(gsubst(polh,0,gmul(pent,polx[0])),gpuigs(pent,6-lambda));
      alpha=(alpha+lambda*itos(ent))&1;
      beta=beta+itos(ent);
      theta=gsub(theta,ent);
    }
  polhp=gmul(gmodulcp(gun,p),polh);
  if(gcmp0(theta))
    {
      fac=factmz(polhp,p);
      maxord=itos((GEN)fac[1]);
      if(maxord>=3)
	{
	  rac=(GEN)fac[2];
	  if(lgef(rac)==5) 
	    {
	      polf[5]=un;
	      goto sortie;
	    } 
	  rac=lift(gdiv(gneg(truecoeff(rac,0)),truecoeff(rac,1)));
	  polh=gsubst(polh,0,gadd(polx[0],rac));
	  lambda=6-maxord; goto labelm6;
	}
      if(maxord<=2&&lgef(polhp)>=7) goto sortie;
    }
  if(lambda<=2)
    { 
      if((myval(truecoeff(polh,2),p)>1-alpha)&&(myval(truecoeff(polh,1),p)>2-alpha)&&(myval(truecoeff(polh,0),p)>3-alpha))
	{
	  pol=gsubst(polh,0,gmul(p,polx[0]));
	  if(alpha) pol=gmul(p,pol); 
	  goto rec1;
	}
      goto sortie;
    }
  if(lambda==3&&alpha==1)
    {
      if(lgef(polhp)==6)
	{
	  if(myval(truecoeff(polh,6),p)>=3&&myval(truecoeff(polh,5),p)>=2)
	    {
	      polh=gmul(gsubst(polh,0,gdiv(polx[0],p)),gpuigs(p,3));
	      theta=gadd(theta,gun);
	      alpha=0;
	      beta=beta-1; 
	      goto sortie; 	      
	    }
	}
      if(lgef(polhp)==9&&!gcmp0(theta))
	{
	  pro=gdiv(polhp,gpuigs(polx[0],3));
	  fac=factmz(pro,p);
	  if(!cmpis((GEN)fac[1],3))
	    {
	      rac=(GEN)fac[2];
	      rac=lift(gdiv(gneg(truecoeff(rac,0)),truecoeff(rac,1)));
	      pro=gsubst(polh,0,gadd(rac,gmul(p,polx[0])));
	      if(polval(pro,p)>=3)
		{
		  polh=gdiv(pro,gpuigs(p,3));
		  alpha=0;
		  beta=beta-1; 
		  theta=caltheta(polh,p,3);
		}
	    }
	}
    }
 sortie:
  polf[1]=(long)polh;
  polf[2]=(long)stoi(lambda);  
  polf[3]=(long)theta;
  polf[4]=(long)stoi(alpha);
  polf[6]=(long)stoi(beta);
  return polf;
}

GEN
caltheta(GEN pol, GEN p, long lambda)
{
  GEN theta,b;
  long i;
  b=cgetg(8,17);
  b[1]=(long)truecoeff(pol,6);b[2]=(long)truecoeff(pol,5);
  b[3]=(long)truecoeff(pol,4);b[4]=(long)truecoeff(pol,3);
  b[5]=(long)truecoeff(pol,2);b[6]=(long)truecoeff(pol,1);
  b[7]=(long)truecoeff(pol,0);
  theta=stoi(myval((GEN)b[2+lambda],p));
  for(i=2+lambda;i<=6;i++)
    theta=gmin(theta,gdivgs(stoi(myval((GEN)b[i+1],p)),i-lambda));
  return theta;
}

long
discpart(GEN polh, GEN p, long ord)   
/* discriminant d'un facteur p-adic de degre 3 */
{
  GEN polhp,list,facti,prod,dis;
  long i,j,fl;
  
  polhp=gmul(gmodulcp(gun,p),polh);
  if(lgef(polhp)!=6) 
    {printf("bug, on ne doit pas arriver dans discpart");fflush(stdout);exit(1);}
  list=factorpadicnonun(polh,p,ord);
  prod=polun[varn(polh)];
  for(i=1;i<lg(list);i++)
    {
      facti=(GEN)list[i];
      fl=0;
      for(j=2;j<lgef(facti);j++) 
	{
	  if(!valp(facti[j])&&j>=3) fl=1;
	}
      if(fl)
	{prod=gmul(prod,facti);} 
    }
  if(lgef(prod)!=6)
    {printf("bug, degre de prod");fflush(stdout);exit(1);}
  dis=discsr(prod);
  if(gcmp0(dis)) return ord+1;
  else return valp(dis);
}

long
myvalzi(GEN b) /* valuation par rapport a 3 dans Z[i] */ 
{
  return min(myval(greal(b),stoi(3)),myval(gimag(b),stoi(3)));
}

GEN 
polyminizi(GEN pol) /* polynome minimal dans Z[i] */
{
  GEN p,polh,ent,pent,unmodp,rac,theta,polf;
  long alpha,beta;

  polf=cgetg(4,18);
  p=stoi(3);
  unmodp=gmodulcp(gun,p);
  alpha=polval(pol,p)&1;
  polh=pol;
  if(alpha) polh=gdiv(pol,p);
  beta=0;  
  rac=gadd(lift(gdiv(gmul(unmodp,truecoeff(polh,3)),gmul(unmodp,truecoeff(polh,6)))),gi);
  
 labelm6:
  polh=gsubst(polh,0,gadd(polx[0],rac));  
  theta=calthetazi(polh);
  if(gcmp(theta,gun)>=0)
    { 
      ent=gfloor(theta);pent=gpui(p,ent,0);
      polh=gdiv(gsubst(polh,0,gmul(pent,polx[0])),gpuigs(pent,3));
      alpha=(alpha+itos(ent))&1;
      beta=beta+itos(ent);
      theta=gsub(theta,ent);
    }
  if(gcmp0(theta))
    {
      if(myvalzi(truecoeff(polh,2))&&myvalzi(truecoeff(polh,1)))
	{
	  rac=gdiv(gmul(unmodp,truecoeff(polh,0)),gmul(unmodp,truecoeff(polh,3)));
	  rac=lift(gneg(gmul(gmul(rac,rac),rac)));
	  goto labelm6;
	}
    }
  if(alpha)
    {
      if(myvalzi(truecoeff(polh,6))>=3&&myvalzi(truecoeff(polh,5))>=2&&myvalzi(truecoeff(polh,4))>=1)
	{
	  polh=gmul(gsubst(polh,0,gdiv(polx[0],p)),gpuigs(p,3));
	  theta=gadd(theta,gun);
	  beta=beta-1;
	  alpha=0;	
	}
    }
  polf[1]=(long)theta;
  polf[2]=(long)stoi(alpha);
  polf[3]=(long)stoi(beta);
  return polf;
}

GEN
calthetazi(GEN polh)
/* on calcule theta (avec lambda=3) pour un polh a coefficients dans Z[i] */
{
  GEN theta;

  theta=gmin(stoi(myvalzi(truecoeff(polh,2))),gmul2n(stoi(myvalzi(truecoeff(polh,1))),-1));
  theta=gmin(theta,gdivgs(stoi(myvalzi(truecoeff(polh,0))),3));
  return theta;
}

long
myvalzi2(GEN b) 
/* on calcule la valuation de b dans Z[i, Y]/(Y^2-3) */
 {
  b=lift(b);
  return min(2*myvalzi(truecoeff(b,0)),2*myvalzi(truecoeff(b,1))+1);
}

GEN
calthetazi2(GEN polh)   
/* on calcule theta (avec lambda=3) d'un polh a coefficients 
dans Z[i, Y]/(Y^2-3) */
{
  GEN theta;

  theta=gmin(stoi(myvalzi2(truecoeff(polh,2))),gmul2n(stoi(myvalzi2(truecoeff(polh,1))),-1));
  theta=gmin(theta,gdivgs(stoi(myvalzi2(truecoeff(polh,0))),3));
  return theta;
}

GEN 
polyminizi2(GEN pol)   /* polynome minimal dans Z_3[i,Y]/(Y^2-3) */
{
  GEN p,polh,ent,pent,unmodp,rac,theta,polf,y,b3,b6;
  long alpha,beta;
  
  p=stoi(3);
  polf=cgetg(3,18);
  y=lisexpr("mod(y,y^2-3)");
  unmodp=gmodulcp(gun,p);
  if(polval(pol,p)) 
    {printf("bug, le polynome n'est pas minimal\n");fflush(stdout);exit(1);}
  polh=pol;alpha=0;
  beta=0;  
  polh=gdivgs(gsubst(polh,0,gmul(polx[0],y)),27);
  if(myvalzi2(truecoeff(polh,4))>0&&myvalzi2(truecoeff(polh,2))>0)  
    {
      if(myvalzi2(gsub(truecoeff(polh,6),truecoeff(polh,0)))>0) rac=gi;
      else rac=gun;
    }
  else 
    {
      theta=gzero;
      goto sortie;
    }

 labelm6:
  polh=gsubst(polh,0,gadd(polx[0],rac));  
  theta=calthetazi2(polh);
  if(gcmp(theta,gun)>=0)
    { 
      ent=gfloor(theta);pent=gpui(y,ent,0);
      polh=gdiv(gsubst(polh,0,gmul(pent,polx[0])),gpuigs(pent,3));
      alpha=(alpha+itos(ent))&1;
      beta=beta+itos(ent);
      theta=gsub(theta,ent);
    }
  if(gcmp0(theta))
    {
      if(myvalzi2(truecoeff(polh,2))&&myvalzi2(truecoeff(polh,1)))
	{
	  b3=zi2mod(truecoeff(polh,3));
	  b6=zi2mod(truecoeff(polh,0));
	  rac=gdiv(b6,gneg(b3));
	  rac=lift(gmul(gmul(rac,rac),rac));
	  goto labelm6;
	}
    }
  if(alpha)
    {
      if(myvalzi2(truecoeff(polh,6))>=3&&myvalzi2(truecoeff(polh,5))>=2&&myvalzi2(truecoeff(polh,5))>=1)
	{
	  theta=gadd(theta,gun);
	  beta=beta-1;
	  alpha=0;	
	}
      else
	{
	  printf("bug dans polyminizi2\n");fflush(stdout);exit(1);
	}
    }
 sortie:
  polf[1]=(long)theta;
  polf[2]=(long)stoi(beta);
  return polf;
}

GEN
zi2mod(GEN u)  /* u est un element de Z[i,Y]/(Y^2-3), on calcule u modulo y */ 
{
  GEN a,b,unmodp;

  unmodp=gmodulcp(gun,stoi(3));
  u=truecoeff(lift(u),0);
  a=gmul(unmodp,greal(u));
  b=gmul(unmodp,gimag(u));
  u=gadd(a,gmul(b,gi));
  return u;
}

GEN
factmz(GEN polhp, GEN p)  
/* donne un facteur de multiplicite maximale d'un polynome 
   dans F_p de degre <7 */
{
  GEN dh,z,b,fac;
  long m,i,vx,l;
  
  b=cgetg(3,18);
  m=0;
  z=polhp;
  if(gcmp(p,stoi(5))<=0)
    {
      if(!gcmp0(discsr(polhp))) m=1;
      else
	{
	  fac=(GEN)factmod(polhp,p);
	  for(i=1;i<lg((GEN)fac[2]);i++) 
	    m=max(m,itos(gcoeff(fac,i,2)));
	  for(i=1;i<lg((GEN)fac[1]);i++) 
	    if(!cmpis(gcoeff(fac,i,2),m)) l=i;
	  z=gcoeff(fac,l,1);
	}
      b[1]=(long)stoi(m);
      b[2]=(long)z;
      return b;
    }
  dh=polhp;
  vx=varn(polhp); 
 lab:
  m=m+1; 
  dh=deriv(dh,vx);
  if(lgef(ggcd(z,dh))<=3) goto sort1;
  z=ggcd(z,dh);    
  goto lab;
 sort1:
  if(m>=3&&lgef(z)==5)
    {
      fac=(GEN)factmod(z,p);
      z=gcoeff(fac,1,1);
    }
 sort:
  b[1]=(long)stoi(m);
  b[2]=(long)z;
  return b;
}
